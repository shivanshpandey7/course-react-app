
import CustomizedTables from './components/Table';
import React from 'react';
import './App.css';
import { useSelector } from 'react-redux';
import { coursesList } from './redux/reducer';
import {  Route, Switch} from 'react-router-dom';
import AddCourse from './components/AddCourse';
import EditCourse from './components/EditCourse';
import Header from './components/Header';

function App() {
  const courses = useSelector(coursesList);

  return (

    <div className="App">

    <div>
      <Header/>
    </div>

    <Switch>
      <Route path="/" exact>
        <CustomizedTables list={courses}  />  
      </Route>
      <Route path="/editCourse/:courseId" exact>
        <EditCourse/>
      </Route>
      <Route path="/addNewCourse" exact>
        <AddCourse/>
      </Route>
    </Switch>

      
    </div>
  );
}

export default App;
