import * as React from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Link, useHistory } from "react-router-dom";
import { ButtonGroup } from "@mui/material";
import './Table.css';
import PropTypes from "prop-types";
import Button from "@mui/material/Button";
import { courseActions } from "../redux/reducer";
import { useState } from 'react';
import { useDispatch } from "react-redux";


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 18,
    '&:hover': {
      color: "rgb(73, 175, 209)"
    }
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  "&:last-child td, &:last-child th": {
    border: 0,
    
  },
  
}));


export default function CustomizedTables(props) {
  const dispatch = useDispatch();
  const [deleteConf, setDeleteConf] = useState(false);

  const deleteHandler = (id) => {
    dispatch(courseActions.deleteCourse(id));
  };

  const history = useHistory();
  console.log("deleteConfHandler");
  return (
    <div>
      <h2 style={{textAlign:"left", fontSize: "2.5em"}}>Courses</h2>
      <Link to="/addNewCourse" style={{ textDecoration: "none"}}>
        {" "}
        <Button variant="contained" style={{ marginBottom:"10px"}}> Add New</Button>{" "}
      </Link>

      
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">Title</StyledTableCell>
              <StyledTableCell align="right">Length</StyledTableCell>
              <StyledTableCell align="right">Category</StyledTableCell>
              <StyledTableCell align="right">Author</StyledTableCell>
              <StyledTableCell align="center">Actions</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.list.map((row) => (
              <StyledTableRow key={row.id}>
                <StyledTableCell className="" align="center">{row.title}</StyledTableCell>
                <StyledTableCell align="right">{row.length}</StyledTableCell>
                <StyledTableCell align="right">{row.category}</StyledTableCell>
                <StyledTableCell align="right">{row.author}</StyledTableCell>
                {deleteConf && <div onClick={() => setDeleteConf(false)} id="myModal" className="modal">
                  <div className="modal-content">
                    <p>Are you sure you want to delete {row.title}?</p>
                    <ButtonGroup disableElevation variant="contained">
                      <Button
                        style={{ marginRight: "3px" }}
                        color="primary"
                        onClick={() => deleteHandler(row.id)}
                      >
                        DELETE
                      </Button>
                      <Button
                        color="secondary"
                        onClick={() => setDeleteConf(false)}
                      >
                        CANCEL
                      </Button>
                    </ButtonGroup>
                    </div>
                </div>}

                <StyledTableCell align="center">
              
                  

                  <ButtonGroup disableElevation variant="contained">
                    <Button
                      style={{ marginRight: "3px"}}
                      color="primary"
                      onClick={() => setDeleteConf(true)}
                    >
                      DELETE
                    </Button>
                    <Button
                      color="secondary"
                      onClick={() => history.push(`/editCourse/${row.id}`)}
                    >
                      EDIT
                    </Button>
                  </ButtonGroup>

                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}


CustomizedTables.propTypes = {
  list : PropTypes.array.isRequired
};