import React from 'react';
import { useDispatch } from 'react-redux';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { Button } from '@mui/material';
import { courseActions } from '../redux/reducer';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';

const AddCourse = () => {

    const history = useHistory();

    const dispatch = useDispatch();

    const [title, setTitle] = useState('');
    const [length, setLength] = useState('');
    const [category, setCategory] = useState('');
    const [author, setAuthor] = useState('');

    const submitFormHandler = () => {
        if(title && length && category && author){
            dispatch(courseActions.addCourse({title: title, length: length, category: category, author: author, id : Date.now() }));
            history.push('/');
        }else {
            alert('No field should be blank !!');
        }
    };


  return (
    <div>

<h2 style={{textAlign:"center", fontSize: "2.5em"}}>Add Course</h2>

    <Box style={{marginTop:"70px"}}
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '100ch', margin:'1%'},
      }}
      noValidate
      autoComplete="off"
    >
     
      <TextField id="outlined-basic" label="Title" variant="outlined" onChange={e => setTitle(e.target.value)}/>
      <br/>
      <TextField id="outlined-basic" label="Length" variant="outlined" onChange={e => setLength(e.target.value)}/>
      <br/>
      <TextField id="outlined-basic" label="Category" variant="outlined" onChange={e => setCategory(e.target.value)}/>
      <br/>
      <TextField id="outlined-basic" label="Author" variant="outlined" onChange={e => setAuthor(e.target.value)}/>
      <br/>

      <Button onClick={submitFormHandler}>Submit</Button>

    </Box>


    </div>
  );
};

export default AddCourse;