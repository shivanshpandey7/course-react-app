import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { coursesList, courseActions } from '../redux/reducer';
import { useHistory } from 'react-router-dom';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { Button } from '@mui/material';


const EditCourse = () => {

    const history = useHistory();


    const [title, setTitle] = useState('');
    const [length, setLength] = useState('');
    const [category, setCategory] = useState('');
    const [author, setAuthor] = useState('');

    const dispatch = useDispatch();
    const params = useParams();

     const list = useSelector(coursesList);

    const id = params.courseId;

    useEffect(() => {
      
        list.map(item => {

            if(item.id == id){
                setTitle(item.title);
                setLength(item.length);
                setCategory(item.category);
                setAuthor(item.author);
            }
        });
        
        

    }, []);
    

    const submitFormHandler = () => {
        if(title && length && category && author){
            dispatch(courseActions.editCourse({title: title, length: length, category: category, author: author, id : +id }));
            history.push('/');
        }else {
            alert('No field should be blank !!');
        }
    };

  return (
    <div>
        <h2 style={{textAlign:"center", fontSize: "2.5em"}}>Edit Course</h2>
        <Box style={{marginTop:"100px"}}
        component="form"
        sx={{
            '& > :not(style)': { m: 1, width: '100ch', margin:'1%'},
        }}
        noValidate
        autoComplete="off"
        >
        
        <TextField id="outlined-basic" label="Title" variant="outlined" value={title} onChange={e => setTitle(e.target.value)}/>
        <br/>
        <TextField id="outlined-basic" label="Length" variant="outlined" value={length} onChange={e => setLength(e.target.value)}/>
        <br/>
        <TextField id="outlined-basic" label="Category" variant="outlined" value={category} onChange={e => setCategory(e.target.value)}/>
        <br/>
        <TextField id="outlined-basic" label="Author" variant="outlined" value={author} onChange={e => setAuthor(e.target.value)}/>
        <br/>

        <Button onClick={submitFormHandler}>Submit</Button>

        </Box>
    </div>
  );
};

export default EditCourse;