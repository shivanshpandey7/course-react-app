import { AppBar, makeStyles, Toolbar, Typography } from "@material-ui/core";
import React from 'react';
import './Header.css';

const useStyles = makeStyles(() => ({
    header :{
        backgroundColor: "rgb(14, 233, 233)",
        marginBottom: ""
    },
    logo : {
        fontFamily: "Work Sans, sans-serif",
        fontWeight: 600,
        color: "#FFFEFE",
        textAlign: "left",
        fontSize: "20px"
    }
}));

const Header = () => {
    const {header, logo} = useStyles();
  return (
      <header style={{"margin-bottom": "80px"}}>
        <AppBar className={header}>
            <Toolbar >
                <Typography className={logo}>
                    Home
                </Typography>
            </Toolbar>
        </AppBar>
    </header>
  );
};

export default Header;