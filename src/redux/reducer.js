import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
    coursesList : [{title:"Hello", length: "11.1", category : "Fiction", author:"Rowling", id: 1},
    {title:"Hello", length: "11.1", category : "Fiction", author:"Nuksaan", id: 2},
    {title:"Hello", length: "11.1", category : "Fiction", author:"Loss", id: 3},
    {title:"Hello", length: "11.1", category : "Fiction", author:"Hello", id: 4}
]
};


const coursesReducer = createSlice({

    name : "notes",
    initialState,
    reducers: {
        addCourse(state, action){
            state.coursesList.push(action.payload);
        },
        deleteCourse(state, action){
            state.coursesList = state.coursesList.filter(item => item.id != action.payload);
        },
        editCourse(state, action){
            state.coursesList = state.coursesList.map(item => item.id === action.payload.id ? action.payload : item);   
        },
        
    }

});

export const coursesList = state => state.coursesList;
export const reducer = coursesReducer.reducer;
export const courseActions = coursesReducer.actions;
